#!/bin/bash

set -x

TESTING=1

# POSIX dot command performs lookup in $PATH
PATH="$PWD:$PATH"
. 10-rosa-xfce-config-xdg.sh

failed=0

export XDG_CONFIG_DIRS=""
[ "$(_mk_var)" = "/etc/xdg/rosa-xfce-config:/etc/xdg" ] || failed=$((++failed))
export XDG_CONFIG_DIRS="/etc/xdg"
[ "$(_mk_var)" = "/etc/xdg/rosa-xfce-config:/etc/xdg" ] || failed=$((++failed))
export XDG_CONFIG_DIRS="/etc/foo:/etc/xdg"
[ "$(_mk_var)" = "/etc/xdg/rosa-xfce-config:/etc/foo:/etc/xdg" ] || failed=$((++failed))
export XDG_CONFIG_DIRS="/etc/xdg/rosa-xfce-config"
[ "$(_mk_var)" = "/etc/xdg/rosa-xfce-config:/etc/xdg" ] || failed=$((++failed))
export XDG_CONFIG_DIRS="/etc/xdg/rosa-xfce-config:/etc/xdg"
[ "$(_mk_var)" = "/etc/xdg/rosa-xfce-config:/etc/xdg" ] || failed=$((++failed))

echo "$failed"
exit "$failed"
