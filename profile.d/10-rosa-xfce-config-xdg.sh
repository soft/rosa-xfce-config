#!/bin/bash

# This script will be sourced, it will break other scripts
#set -efu

TESTING="${TESTING:-0}"
XDG_CONFIG_DIRS="${XDG_CONFIG_DIRS:-}"
SYSTEMD_GENERATOR="${SYSTEMD_GENERATOR:-0}"
export ROSA_XDG_CONFIG_DIR="/etc/xdg/rosa-xfce-config"

_mk_var(){
	if [[ "$XDG_CONFIG_DIRS" =~ .*(^|:)${ROSA_XDG_CONFIG_DIR} ]]; then
		if [[ "$XDG_CONFIG_DIRS" =~ .*(^|:)/etc/xdg(:|$) ]]; then
			echo "$XDG_CONFIG_DIRS"
			return 0
		fi
		echo "${XDG_CONFIG_DIRS}:/etc/xdg"
		return 0
	fi
	if [ -z "$XDG_CONFIG_DIRS" ]; then
		echo "${ROSA_XDG_CONFIG_DIR}:/etc/xdg"
		return 0
	fi
	# if $XDG_CONFIG_DIRS is already set
	if [[ "$XDG_CONFIG_DIRS" =~ .*(^|:)/etc/xdg(:|$) ]]; then
		echo "${ROSA_XDG_CONFIG_DIR}:${XDG_CONFIG_DIRS}"
		return
	fi
	echo "${ROSA_XDG_CONFIG_DIR}:${XDG_CONFIG_DIRS}:/etc/xdg"
	return
}

_main(){
	local dir="$(_mk_var)"
	if [ -n "$dir" ]; then
		if [ "$SYSTEMD_GENERATOR" = 1 ]
		then
			echo XDG_CONFIG_DIRS="$dir"
		else
			export XDG_CONFIG_DIRS="$dir"
		fi
	fi
}

if [ "$TESTING" = 0 ]; then
	_main
	unset ROSA_XDG_CONFIG_DIR
fi
