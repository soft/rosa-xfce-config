#!/bin/sh
# Disable showing removable devices on desktop in LiveCD,
# otherwise there are too many useless icons of squahsfs etc.
# To be run by Anaconda,
# Anaconda rsync's original LiceCD image without this change.
# Authors:
# - Mikhail Novosyolov <m.novosyolov@rosalinux.ru>, 2021

set -xefu

file="/etc/xdg/rosa-xfce-config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml"
test -f "$file"
grep -n '<!-- LIVECD:' "$file" | while read -r line
do
	line_number="$(echo "$line" | awk -F ':' '{print $1}')"
	sed_pattern="$(echo "$line" | awk -F '<!-- LIVECD: sed: ' '{print $2}' | sed -e 's, -->$,,')"
	if [ -n "$line_number" ] && [ -n "$sed_pattern" ]; then
		sed -i'' -e "${line_number}${sed_pattern}" "$file"
	fi
done
