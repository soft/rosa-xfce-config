# 2012 Author: tpg@mandriva.org
# 2014 Author: tpgxyz@gmail.com
# 2017 Author: vladi105@yandex.ru
# 2020 Author: vladi105@yandex.ru
# 2021 Author: m.novosyolov@rosalinux.ru

PACKAGE = rosa-xfce-config
VERSION = `date +%Y%m%d`

PREFIX ?= /usr
SYSCONFDIR ?= /etc
XDGCONFDIR = $(SYSCONFDIR)/xdg/rosa-xfce-config

# %_fontconfig_templatedir
FONTCONFIG_TEMPLATEDIR ?= /usr/share/fontconfig/conf.avail
# %_fontconfig_confdir
FONTCONFIG_CONFDIR ?= /etc/fonts/conf.d

install:
	-install -d $(DESTDIR)$(XDGCONFDIR)
	-install -d $(DESTDIR)$(XDGCONFDIR)/Terminal
	install -m 644 common/Terminal/* $(DESTDIR)$(XDGCONFDIR)/Terminal/
	-install -d $(DESTDIR)$(XDGCONFDIR)/menus
	install -m 644 common/menus/xfce-applications.menu $(DESTDIR)$(XDGCONFDIR)/menus/xfce-applications.menu
	-install -d $(DESTDIR)$(XDGCONFDIR)/Thunar
	install -m 644 common/Thunar/* $(DESTDIR)$(XDGCONFDIR)/Thunar/
	-install -d $(DESTDIR)$(XDGCONFDIR)/autostart
	install -m 644 common/autostart/* $(DESTDIR)$(XDGCONFDIR)/autostart/
	-install -d $(DESTDIR)$(XDGCONFDIR)/xfce4
	-install -d $(DESTDIR)$(XDGCONFDIR)/xfce4/desktop
	-install -d $(DESTDIR)$(XDGCONFDIR)/xfce4/panel
	-install -d $(DESTDIR)$(XDGCONFDIR)/xfce4/xfconf/xfce-perchannel-xml
	-install -d $(DESTDIR)$(PREFIX)/bin
	cp -fr common/xfce4/panel/* $(DESTDIR)$(XDGCONFDIR)/xfce4/panel/
	install -m 644 common/xfce4/xfconf/xfce-perchannel-xml/* $(DESTDIR)$(XDGCONFDIR)/xfce4/xfconf/xfce-perchannel-xml/
	install -m 644 common/xfce4/*.rc $(DESTDIR)$(XDGCONFDIR)/xfce4
	install -m 755 tools/xfce4-firstrun $(DESTDIR)$(PREFIX)/bin
	install -m 755 tools/xfce-reset $(DESTDIR)$(PREFIX)/bin
	-install -d $(DESTDIR)$(SYSCONFDIR)
	-install -d $(DESTDIR)$(XDGCONFDIR)/xfce4/xfconf/xfce-perchannel-xml/
	install -m 644 common/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml $(DESTDIR)$(XDGCONFDIR)/xfce4/xfconf/xfce-perchannel-xml/
	install -m 644 common/mimeapps.list $(DESTDIR)$(XDGCONFDIR)/mimeapps.list

	cd profile.d && bash test_10-rosa-xfce-config-xdg.sh
	-install -d $(DESTDIR)$(SYSCONFDIR)/profile.d/
	install -m 755 profile.d/10-rosa-xfce-config-xdg.sh $(DESTDIR)$(SYSCONFDIR)/profile.d/10-rosa-xfce-config-xdg.sh
	install -d $(DESTDIR)$(PREFIX)/lib/systemd/user-environment-generators
	install -m 755 profile.d/10-rosa-xfce-config-systemd-env.sh $(DESTDIR)$(PREFIX)/lib/systemd/user-environment-generators/10-rosa-xfce-config-systemd-env.sh

	-install -d $(DESTDIR)$(PREFIX)/share/wallpapers
	install -m 644 images/rosa-xfce-1.jpg $(DESTDIR)$(PREFIX)/share/wallpapers/rosa-xfce-1.jpg
	# test that file is not corrupted so that the script will not work
	grep -q '<!-- LIVECD: sed: s/true/false/ -->' common/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml
	-install -d $(DESTDIR)/etc/anaconda-scripts.d/livecd-init/
	install -m 755 images/livecd-fix-xfce-desktop.sh $(DESTDIR)/etc/anaconda-scripts.d/livecd-init/livecd-fix-xfce-desktop.sh

	# Install fonts, based on fonts-ttf-liberation
	-install -d $(DESTDIR)$(FONTCONFIG_TEMPLATEDIR)
	-install -d $(DESTDIR)$(FONTCONFIG_CONFDIR)
	# Note: .xml -> .conf
	install -m 644 fontconfig/40-rosa-xfce-config-monospace.xml $(DESTDIR)$(FONTCONFIG_TEMPLATEDIR)/40-rosa-xfce-config-monospace.conf
	ln -s $(FONTCONFIG_TEMPLATEDIR)/40-rosa-xfce-config-monospace.conf $(DESTDIR)$(FONTCONFIG_CONFDIR)/40-rosa-xfce-config-monospace.conf

	-install -d $(DESTDIR)$(PREFIX)/share/rosa-xfce-config
	install -m 644 images/rosa-logo-16x16.svg $(DESTDIR)$(PREFIX)/share/rosa-xfce-config/rosa-logo-16x16.svg

dist:
	git archive --format=tar --prefix=$(PACKAGE)-$(VERSION)/ HEAD | xz -2vec -T0 > $(PACKAGE)-$(VERSION).tar.xz;
	$(info $(PACKAGE)-$(VERSION).tar.xz is ready)

.PHONY: ChangeLog log changelog

log: ChangeLog

changelog: ChangeLog


ChangeLog:
	@if test -d "$$PWD/.git"; then \
	  git --no-pager log --format="%ai %aN %n%n%x09* %s%d%n" > $@.tmp \
	  && mv -f $@.tmp $@ \
	  && git commit ChangeLog -m 'generated changelog' \
	  && if [ -e ".git/svn" ]; then \
	    git svn dcommit ; \
	    fi \
	  || (rm -f  $@.tmp; \
	 echo Failed to generate ChangeLog, your ChangeLog may be outdated >&2; \
	 (test -f $@ || echo git-log is required to generate this file >> $@)); \
	else \
	 svn2cl --accum --authors ../common/username.xml; \
	 rm -f *.bak;  \
	fi;
